package com.example.piechart;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    PieChart pieChart;
    int[] colorClassArray = new int[]{Color.rgb(105, 245, 140),Color.rgb(105, 245, 205),Color.rgb(105, 180, 245), Color.rgb(105, 124, 245),Color.rgb(240, 137, 217),Color.rgb(255, 158, 158)};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pieChart = findViewById(R.id.pieChart);
        PieDataSet pieDataSet = new PieDataSet(dataValues1(),"");
        pieDataSet.setColors(colorClassArray);
        pieDataSet.setValueTextSize(10);


        PieData pieData = new PieData(pieDataSet);

        pieChart.setData(pieData);
        pieChart.setUsePercentValues(true);
        pieChart.setCenterText("PieChart Ejemplo");
        pieChart.setCenterTextSize(20);
        pieChart.setCenterTextColor(Color.rgb(56, 50, 50));
        pieChart.invalidate();
    }

    private ArrayList<PieEntry> dataValues1(){
        ArrayList<PieEntry> dataVals = new ArrayList<>();

        dataVals.add(new PieEntry(15,"Dom"));
        dataVals.add(new PieEntry(34,"Lun"));
        dataVals.add(new PieEntry(25,"Mar"));
        dataVals.add(new PieEntry(10,"Mie"));
        dataVals.add(new PieEntry(40,"Jue"));
        dataVals.add(new PieEntry(6,"Vie"));
        dataVals.add(new PieEntry(28,"Sab"));
        return dataVals;
    }
}
